<?php
	$fonts="verdana";
	$bgcolor="#444";
?>

<!doctype html>

<html>
	<head>
		<title>PHP Syntax</title>
		<style>
		body{font-family:<?php echo $fonts;?>}
			.phpcoding{width:900px;margin:0 auto;background:#ddd;}
			.headeroption, .footeroption{background:<?php echo $bgcolor;?>;color:#fff;padding:20px;text-align:center;}
			.headeroption h2, .footeroption h2{margin:0;}
			.maincontent{padding:20px;min-height:400px;}
			p{
				margin: 0;
			}
			h2{
				margin: 0;
				font-size: 24px;
			}
			input[type="text"]{
				width: 238px;
				padding: 5px;
			}
			select{
				font-size: 18px;
				padding: 2px 5px;
				width: 250px;
			}
			#myform{
				width: 400px;
				border: 1px solid #fff;padding: 10px;
			}

			
			.tblone {
   				 width: 420px;
  				 border: 1px solid #f1f1f1;
   				 margin: 20px 0px;
			}
			.tblone td {
  				  padding: 5px 10px;
			}
			table.tblone tr:nth-child(2n+1){
				background: #fff;
				height: 30px;
			}
			table.tblone tr:nth-child(2n){
				background: #f1f1f1;
				height: 30px;
			}

		</style>
	</head>
	
	<body>
	
			<div class="phpcoding">
			
				<section class="headeroption">
					<h2>Working with html forms</h2>
				</section>
				
<section class="maincontent">
				
				<hr/>
					Get data from a data table in php
				<hr/>